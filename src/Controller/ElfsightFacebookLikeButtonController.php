<?php

namespace Drupal\elfsight_facebook_like_button\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * {@inheritdoc}
 */
class ElfsightFacebookLikeButtonController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function content() {
    $url = 'https://apps.elfsight.com/embed/facebook-like-button/?utm_source=portals&utm_medium=drupal&utm_campaign=facebook-like-button&utm_content=sign-up';

    require_once __DIR__ . '/embed.php';

    return [
      'response' => 1,
    ];
  }

}
